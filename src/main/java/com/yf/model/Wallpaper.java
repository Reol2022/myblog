package com.yf.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Describe:壁纸
 */
@Data
@NoArgsConstructor
public class Wallpaper {
    /**
     * 壁纸链接
     */
    private String url;


}
