package com.yf.service;

import com.yf.model.DailySpeech;
import com.yf.utils.DataMap;

/**
 * Describe: 藏心阁-今日
 */
public interface TodayService {

    DataMap publishISay(DailySpeech dailySpeech);

    DataMap getTodayInfo(int rows, int pageNum);

}
