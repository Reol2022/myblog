package com.yf.service.impl;

import com.yf.mapper.WallpaperMapper;
import com.yf.model.Wallpaper;
import com.yf.service.WallpaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WallpaperServiceImpl implements WallpaperService{
    @Autowired
    WallpaperMapper wallpaperMapper;
    @Autowired
    WallpaperService wallpaperService;

    @Override
    public List<Wallpaper> findWallpaper(){
        return wallpaperMapper.WallpaperUrl();
    }


}
