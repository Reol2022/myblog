package com.yf.service;

import com.yf.model.Wallpaper;

import java.util.List;

/**
 * Describe: 壁纸
 */
public interface WallpaperService {

    List<Wallpaper> findWallpaper();
}
