package com.yf.controller;

import com.alibaba.fastjson.JSON;
import com.yf.mapper.WallpaperMapper;
import com.yf.service.WallpaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WallpaperControl {
    @Autowired
    WallpaperMapper wallpaperMapper;
    @Autowired
    WallpaperService wallpaperService;

    @GetMapping(value = "/wallpaper", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String findWallpaper(){
        String string = JSON.toJSONString(wallpaperService.findWallpaper());
        return string;
    }
}
