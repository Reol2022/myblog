package com.yf.mapper;

import com.yf.model.Article;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 搜索文章
 */
@Mapper
@Repository
public interface SearchArticleMapper {
    @Select("SELECT * FROM article WHERE articleTitle LIKE concat('%',#{articleTitle},'%')")
    List<Article> SearchArti(String articleTitle);
}
