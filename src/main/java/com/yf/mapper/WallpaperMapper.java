package com.yf.mapper;

import com.yf.model.Wallpaper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Describe: 壁纸sql
 */
@Mapper
@Repository
public interface WallpaperMapper {
    @Select("select url from wallpaper;")
    List<Wallpaper> WallpaperUrl();
}
