package com.yf.constant;

/**
 * Describe: 博客网站超级管理员
 */
public interface SiteOwner {

    /**
     * 填写超级管理员的用户名
     */
    String SITE_OWNER = "严飞";

    /**
     * 填写网站域名或ip地址
     */
    String SITE_OWNER_URL = "https:osako.top";

}
